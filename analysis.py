import argparse
import pickle as pk
import pandas as pd


def save_analysis_result(test_fname, save_fname, analysis_fname):
    id2label = {0: "negative", 1: "neutral", 2: "positive"}
    best_y_pred = pk.load(open(save_fname, 'rb'))
    test_f = open(test_fname, 'r')
    analysis_result = []
    i = 0
    while True:
        line = test_f.readline()
        if not line:
            break
        sentence = line.strip()
        aspect_num = int(test_f.readline().strip())
        for _ in range(aspect_num):
            aspect = test_f.readline().strip()
            ground_truth = test_f.readline().strip()
            analysis_result.append((sentence, aspect, ground_truth, id2label[best_y_pred[i]],
                                    ground_truth == id2label[best_y_pred[i]]))
            i += 1
    df = pd.DataFrame(analysis_result)
    df.columns = ["sentence", "aspect", "ground_truth", "predict_label", "result"]
    df.to_csv(analysis_fname)
    assert i == len(best_y_pred)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--dataset', default='data/restaurant/', type=str, help='data set')
    args = parser.parse_args()

    test_fname = args.dataset + '/test.txt'
    save_fname = args.dataset + '/predict.pk'
    analysis_fname = args.dataset + '/analysis.csv'

    print('Saving analysis result ...')
    save_analysis_result(test_fname, save_fname, analysis_fname)
